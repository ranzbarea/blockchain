package utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;

import org.bitcoinj.core.Base58;

import components.Transaction;

public class StringUtil {

	//Applies Sha256 to a string and returns the result.
	public static String applySha256(String input){
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");

			byte[] hash = digest.digest(
					input.getBytes(StandardCharsets.UTF_8));

			StringBuilder hexString = new StringBuilder(2 * hash.length);
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Applies ECDSA Signature and returns the result ( as bytes ).
	public static byte[] applyECDSASig(PrivateKey privateKey, String input) {
		Signature dsa;
		byte[] output = new byte[0];
		try {
			dsa = Signature.getInstance("ECDSA", "BC");
			dsa.initSign(privateKey);
			byte[] strByte = input.getBytes();
			dsa.update(strByte);
			byte[] realSig = dsa.sign();
			output = realSig;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return output;
	}

	//Verifies a String signature 
	public static boolean verifyECDSASig(PublicKey publicKey, String data, byte[] signature) {
		try {
			Signature ecdsaVerify = Signature.getInstance("ECDSA", "BC");
			ecdsaVerify.initVerify(publicKey);
			ecdsaVerify.update(data.getBytes());
			return ecdsaVerify.verify(signature);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getStringFromKey(Key key) {
		return Base64.getEncoder().encodeToString(key.getEncoded());
	}

	public static PrivateKey getPrivateKeyFromString(Object key) {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		String strkey = (String) key;
		byte[] byteKey = Base64.getDecoder().decode(strkey);
		KeyFactory factory= null;
		try {
			factory = KeyFactory.getInstance("ECDSA", "BC");
			return factory.generatePrivate(new PKCS8EncodedKeySpec(byteKey));
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static PublicKey getPublicKeyFromString(Object key) {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		String strkey = (String) key;
		byte[] byteKey = Base64.getDecoder().decode(strkey);
		KeyFactory factory= null;
		try {
			factory = KeyFactory.getInstance("ECDSA", "BC");
			return factory.generatePublic(new X509EncodedKeySpec(byteKey));
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}


	//Tacks in array of transactions and returns a merkle root.
	public static String getMerkleRoot(ArrayList<Transaction> transactions) {
		int count = transactions.size();
		ArrayList<String> previousTreeLayer = new ArrayList<String>();
		for(Transaction transaction : transactions) {
			previousTreeLayer.add(transaction.transactionId);
		}
		ArrayList<String> treeLayer = previousTreeLayer;
		while(count > 1) {
			treeLayer = new ArrayList<String>();
			for(int i=1; i < previousTreeLayer.size(); i++) {
				treeLayer.add(applySha256(previousTreeLayer.get(i-1) + previousTreeLayer.get(i)));
			}
			count = treeLayer.size();
			previousTreeLayer = treeLayer;
		}
		String merkleRoot = (treeLayer.size() == 1) ? treeLayer.get(0) : "";
		return merkleRoot;
	}

	//Returns difficulty string target, to compare to hash. eg difficulty of 5 will return "00000"  
	public static String getDificultyString(int difficulty) {
		return new String(new char[difficulty]).replace('\0', '0');
	}


	public static String getAddress(PublicKey publicKey){
		//Encoding publicKey into a Bitcoin address
		ECPublicKey epub = (ECPublicKey)publicKey;
		//ECDSA is represented by a point on an elliptical curve with X and Y coordinates
		ECPoint pt = epub.getW();
		String sx = adjustTo64(pt.getAffineX().toString(16)).toUpperCase();
		String sy = adjustTo64(pt.getAffineY().toString(16)).toUpperCase();
		String bcPub = "04" + sx + sy;

		//Perform SHA-256 hash
		MessageDigest sha = null;
		try {
			sha = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] s1 = null;
		try {
			s1 = sha.digest(bcPub.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Perform RIPEMD-160 Hash
		MessageDigest ripemd160 = null;
		try {
			ripemd160 = MessageDigest.getInstance("RipeMD160", "BC");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] r1 = ripemd160.digest(s1);
		//Add a version byte of 0x00 at the beginning of the hash because we will be always in Main Network.
		byte[] r2 = new byte[r1.length + 1];
		r2[0] = 0;
		for (int i = 0 ; i < r1.length ; i++) r2[i+1] = r1[i];

		//On the result above we perform SHA-256 twice
		byte[] s2 = sha.digest(r2);
		byte[] s3 = sha.digest(s2);

		//Take the first 4 bytes of the second SHA-256 hash as the address checksum
		//and add these 4 bytes to the end of the RIPEMD-160 hash getting a 25-byte address
		byte[] a1 = new byte[25];
		for (int i = 0 ; i < r2.length ; i++) a1[i] = r2[i];
		for (int i = 0 ; i < 5 ; i++) a1[20 + i] = s3[i];

		//Finally we encode the address using Bitcoinj Base58.
		return Base58.encode(a1);
	}

	/*
	 * Sets the hexadecimal string to be 64 characters in length
	 */
	private static String adjustTo64(String s) { //FIXME:Review how works the adjustTo64, the necessarylength
		switch(s.length()) {
		case 62: return "00" + s;
		case 63: return "0" + s;
		case 64: return s;
		default:
			throw new IllegalArgumentException("not a valid key: " + s); //FIXME: Check if it is necessary to adjust length to 64
		}
	}
}
