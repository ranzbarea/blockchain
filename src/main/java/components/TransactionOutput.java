package components;

import java.math.BigDecimal;

import org.bson.Document;

import utils.StringUtil;

public class TransactionOutput {

	public String id;
	public String reciepient; //also known as the new owner of these coins.
	public BigDecimal value; //the amount of coins they own
	public String parentTransactionId; //the id of the transaction this output was created in

	//Constructor
	public TransactionOutput(String parentTransactionId, String reciepientAddress, BigDecimal value) {
		this.reciepient = reciepientAddress;
		this.value = value;
		this.parentTransactionId = parentTransactionId;
		this.id = StringUtil.applySha256(reciepient+value.toString()+parentTransactionId);
	}

	//Check if coin belongs to you
	public boolean isMine(String address) {
		return (address.equals(reciepient));
	}

	public Document getTransactionOutputDocument(){
		Document document = new Document()
				.append("id", id)
				.append("reciepient", reciepient)
				.append("value", value)
				.append("parentTransactionId", parentTransactionId);
		return document;
	}

}
