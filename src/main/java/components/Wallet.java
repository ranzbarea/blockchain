package components;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.*;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bitcoinj.core.Base58;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBList;
import com.mongodb.client.model.ReplaceOptions;

import managers.DBManager;

import com.mongodb.client.FindIterable;

import static com.mongodb.client.model.Filters.eq;

import utils.StringUtil;

public class Wallet {

	public PrivateKey privateKey;
	public PublicKey publicKey;
	public String address;
	public HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>(); //UTXOs owned by this wallet.
	//	public HashMap<String,TransactionOutput> processingUTXOs = new HashMap<String,TransactionOutput>();
	private DBManager dbManager;

	public Wallet(){
		generateKeyPair();
		address = StringUtil.getAddress(publicKey);

		//Adding this wallet for the first time
		dbManager = new DBManager();
		dbManager.insertOne("wallets", getDocumentWallet());
	}

	public Wallet(String id) {
		restoreWallet(id);
		dbManager = new DBManager();
	}

	private void restoreWallet(String id) {
		Document wallet = dbManager.findFirst("wallets",new Document("privateKey",id));

		privateKey = StringUtil.getPrivateKeyFromString(wallet.get("privateKey"));
		publicKey = StringUtil.getPublicKeyFromString(wallet.get("publicKey"));
		address = wallet.getString("address");

		UTXOs.clear();
		ArrayList<Document> utxos = (ArrayList<Document>) wallet.get("UTXOs");
		for(int i = 0; i < utxos.size();i++) {
			UTXOs.put(utxos.get(i).getString("id"), new TransactionOutput(utxos.get(i).getString("parentTransactionId"),utxos.get(i).getString("reciepient"), ((org.bson.types.Decimal128) utxos.get(i).get("value")).bigDecimalValue()));
		}

	}

	private void generateKeyPair() {//FIXME: Review how to generate keyPair
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA","BC");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			ECGenParameterSpec ecSpec = new ECGenParameterSpec("secp256k1");

			// Initialize the key generator and generate a KeyPair
			keyGen.initialize(ecSpec, random);   //256 bytes provides an acceptable security level
			KeyPair keyPair = keyGen.generateKeyPair();
			// Set the public and private keys from the keyPair
			privateKey = keyPair.getPrivate();
			publicKey = keyPair.getPublic();

		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//returns balance and stores the UTXO's owned by this wallet in this.UTXOs
	public BigDecimal getBalance() {
		refresh();
		BigDecimal total = new BigDecimal(0);	

		for (Map.Entry<String, TransactionOutput> item: UTXOs.entrySet()){ //ORIGINAL CODE
			TransactionOutput UTXO = item.getValue();
			if(UTXO.isMine(address)) { //if output belongs to me ( if coins belong to me )
				total = total.add(UTXO.value);
			}
		}  
		return total.setScale(8, RoundingMode.HALF_DOWN); //TODO: It is necessary to set the scale here?
	}

	private void refresh() {//Only refresh the UTXOs
		FindIterable<Document> iter = dbManager.findAll("utxos", eq("reciepient",address));
		for(Document doc : iter) {//FIXME: Create parse class //Add UTXOs to our list of unspent transactions
			if(!UTXOs.containsKey(doc.getString("id"))) {//add it to our list of unspent transactions.
				UTXOs.put(doc.getString("id"), new TransactionOutput(doc.getString("parentTransactionId"),doc.getString("reciepient"), ((org.bson.types.Decimal128) doc.get("value")).bigDecimalValue() ));
			}
		}
		updateWallet();
	}

	//Generates and returns a new transaction from this wallet.
	public Transaction sendFunds(String _recipient,BigDecimal value ) {
		refresh();

		if(value.scale()>9) {//Check if the number of decimals in value is not out of limits
			System.out.println("Decimals of the variable out of bounds.");
			return null;
			//			value = new BigDecimal(value).setScale(8, RoundingMode.HALF_DOWN).doubleValue(); //TODO: Round down by default?
		}
		if(getBalance().compareTo(value) == -1) { //gather balance and check funds.
			System.out.println("#Not Enough funds to send transaction. Transaction Discarded.");
			return null;
		}
		//create array list of inputs
		ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();

		BigDecimal total = new BigDecimal(0);
		for (Map.Entry<String, TransactionOutput> item: UTXOs.entrySet()){
			TransactionOutput UTXO = item.getValue();
			total = total.add(UTXO.value);
			inputs.add(new TransactionInput(UTXO.id));
			if(total.compareTo(value) == 1) break;
		}

		Transaction newTransaction = new Transaction(publicKey, _recipient , value, inputs);
		newTransaction.generateSignature(privateKey);

		//once the transaction has been successfully created, the UTXO's from our wallet are removed from our storage.
		//FIXME: We should delete them when the transaction has been verified by the blockchain?
		for(TransactionInput input: inputs){
			UTXOs.remove(input.transactionOutputId);
		}

		return newTransaction;
	}

	public void updateWallet() {
		Bson query = eq("privateKey", StringUtil.getStringFromKey(privateKey));
		dbManager.replaceOne("wallets", query, getDocumentWallet(), new ReplaceOptions());
	}

	private Document getDocumentWallet(){
		Document document = new Document()
				.append("privateKey", StringUtil.getStringFromKey(privateKey))
				//				.append("privateKey", privateKey)
				.append("publicKey", StringUtil.getStringFromKey(publicKey))
				.append("address", address);

		BasicDBList utxos = new BasicDBList();
		for(Entry<String,TransactionOutput> entry : UTXOs.entrySet()) {
			utxos.add(entry.getValue().getTransactionOutputDocument());
		}

		document.append("UTXOs", utxos);

		return document;
	}

}