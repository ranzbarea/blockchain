package components;

import org.bson.Document;

public class TransactionInput {
	public String transactionOutputId; //Reference to TransactionOutputs -> transactionId
	public TransactionOutput UTXO; //Contains the Unspent transaction output

	public TransactionInput(String transactionOutputId) {
		this.transactionOutputId = transactionOutputId;
	}

	public Document getJSONTransactionInput() {
		Document document = new Document()
				.append("transactionOutputId", transactionOutputId)
				.append("UTXO", UTXO.getTransactionOutputDocument());
		return document;
	}
}
