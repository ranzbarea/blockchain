package components;

import java.math.BigDecimal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBList;

import utils.StringUtil;

public class Transaction {

	public String transactionId; // this is also the hash of the transaction.
	public PublicKey sender; // senders address/public key.
	public String senderAddress;
	//	public PublicKey senderChange;
	public String reciepient; // Recipients address/public key.
	public BigDecimal value;
	public byte[] signature; // this is to prevent anybody else from spending funds in our wallet.

	public ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();
	public ArrayList<TransactionOutput> outputs = new ArrayList<TransactionOutput>();

	private static int nonce = 0; // a rough count of how many transactions have been generated. 

	// Constructor: 
	public Transaction(PublicKey from, String addressReciepient, BigDecimal value,  ArrayList<TransactionInput> inputs) {
		this.sender = from;
		this.senderAddress = StringUtil.getAddress(sender);
		this.reciepient = addressReciepient;
		if(!isValidFormatOfValue(value)) {//Check if the number of decimals in value is not out of limits
			throw new RuntimeException("Decimals of the variable out of bounds."); //FIXME: It is necessary?
		}
		this.value = value;
		this.inputs = inputs;
	}

	// This Calculates the transaction hash (which will be used as its Id)
	private String calulateHash() {
		nonce++; //increase the nonce to avoid 2 identical transactions having the same hash
		return StringUtil.applySha256(
				senderAddress +
				reciepient +
				value.toString()+ 
				nonce
				);
	}

	//Signs all the data we dont wish to be tampered with.
	public void generateSignature(PrivateKey privateKey) {
		String data = senderAddress + reciepient +value.toString();
		signature = StringUtil.applyECDSASig(privateKey,data);		
	}
	//Verifies the data we signed hasnt been tampered with
	public boolean verifiySignature() {
		String data = senderAddress + reciepient + value.toString();
		return StringUtil.verifyECDSASig(sender, data, signature);
	}

	//Returns true if new transaction could be created.	
	public boolean processTransaction() {

		if(verifiySignature() == false) {
			System.out.println("#Transaction Signature failed to verify.");
			return false;
		}

		if(!isValidFormatOfValue(this.value)) {
			System.out.println("Transaction value out of bounds.");
			return false;
		}

		//gather transaction inputs (Make sure they are unspent):
		for(TransactionInput i : inputs) {
			if(Chain.UTXOs.get(i.transactionOutputId) == null) {//FIXME: Is this if necessary? (NO???)
				inputs.remove(i);//FIXME: Delete inputs that are not spendable here?
				continue;
			}
			if(!isValidFormatOfValue(Chain.UTXOs.get(i.transactionOutputId).value)) {
				System.out.println("El formato del valor del input no es v�lido.");
				return false;
			}
			i.UTXO = Chain.UTXOs.get(i.transactionOutputId);
		}

		//check if transaction is valid:
		if(getInputsValue().compareTo(Chain.minimumTransaction) == -1) {//If inputs value is less than minimumTransaction...
			System.out.println("#Transaction Inputs to small: " + getInputsValue().toString());
			return false;
		}

		//generate transaction outputs:
		//		double leftOver = getInputsValue() - value; //get value of inputs then the left over change:
		BigDecimal leftOver = getInputsValue().subtract(value);//get value of inputs then the left over change:
		transactionId = calulateHash();
		outputs.add(new TransactionOutput( transactionId, this.reciepient, value)); //send value to recipient
		outputs.add(new TransactionOutput( transactionId, senderAddress, leftOver)); //send the left over 'change' back to sender		

		//FIXME:add outputs to Unspent list (i am not sure)
		for(TransactionOutput o : outputs) { //FIXME: add to the database
			Chain.UTXOs.put(o.id , o);
			if(!isValidFormatOfValue(o.value)) {
				System.out.println();
			}
		}

		//FIXME: remove transaction inputs from UTXO lists as spent:
		for(TransactionInput i : inputs) {
			if(i.UTXO == null) continue; //if Transaction can't be found skip it 
			Chain.UTXOs.remove(i.UTXO.id);//FIXME: remove from database
		}

		return true;
	}

	private boolean isValidFormatOfValue(BigDecimal value) {
		return value.scale()<9; //Check if the number of decimals in value is not out of limits
	}

	//returns sum of inputs(UTXOs) values
	public BigDecimal getInputsValue() {
		BigDecimal total = new BigDecimal(0);
		for(TransactionInput input : inputs) {
			if(input.UTXO == null) {
				//				inputs.remove(input.UTXO.id); //FIXME: UNCOMMENT THIS LINE or not?
				continue; //if Transaction can't be found skip it 
			}
			total = total.add(input.UTXO.value);
		}
		return total;
	}

	//returns sum of outputs:
	public BigDecimal getOutputsValue() {
		BigDecimal total = new BigDecimal(0);
		for(TransactionOutput o : outputs) {
			total = total.add(o.value);
		}
		return total;
	}

	public Document getTransactionDocument() {

		Document document = new Document()
				.append("transactionId", transactionId)
				.append("sender", senderAddress)
				.append("reciepient", reciepient)
				.append("value", value)
				.append("signature", signature);
		//				.append("inputs", inputs)
		//				.append("outputs", outputs);
		BasicDBList transactionsInputs = new BasicDBList();
		if(inputs!=null) {
			for(int i = 0 ; i<inputs.size() ; i++) {
				transactionsInputs.add(inputs.get(i).getJSONTransactionInput());
			}
		}
		document.append("transactionsInputs", transactionsInputs);

		BasicDBList transactionsOutputs = new BasicDBList();
		if(outputs!=null) {
			for(int i = 0 ; i<outputs.size() ; i++) {
				transactionsOutputs.add(outputs.get(i).getTransactionOutputDocument());
			}
		}
		document.append("transactionsOutputs", transactionsOutputs);

		return document;
	}

	public List<Document> getTransactionOutputsDocument(){
		List<Document> outputs = new ArrayList<Document>();
		for(TransactionOutput output : this.outputs) {
			outputs.add(output.getTransactionOutputDocument());
		}

		return outputs;
	}

}
