package components;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.Document;


import com.mongodb.client.result.DeleteResult;

import managers.DBManager;

import static com.mongodb.client.model.Filters.eq;
import org.bson.conversions.Bson;

import utils.StringUtil;

public class Chain {

	public static HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>(); //list of all unspent transactions. 
	public static ArrayList<Block> blockchain = new ArrayList<Block>();
	//Difficulty set the number of 0 required at the start of the hash.
	public static int difficulty = 2;
	public static Wallet walletA;
	public static Wallet walletB;
	public static BigDecimal minimumTransaction = new BigDecimal(0.00000001);
	public static Transaction genesisTransaction;
	private static DBManager dbManager;

	public static void main(String[] args) {
		dbManager= new DBManager();

		//Setup Bouncy castle as a Security Provider
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		//Create the new wallets
		walletA = new Wallet();
		walletB = new Wallet();

		generateGenesisBlock();

		//Testing random
		for(int i = 1 ; i < 200 ; i++) {
			System.out.println("Creating and Mining block: "+i +"... ");
			Block block = new Block(blockchain.get(blockchain.size()-1).hash);
			block.addTransaction(walletA.sendFunds(walletB.address, (new BigDecimal(Math.random()*100)).setScale(8, RoundingMode.HALF_DOWN) ));
			block.addTransaction(walletB.sendFunds(walletA.address, (new BigDecimal(Math.random()*100)).setScale(8, RoundingMode.HALF_DOWN) ));
			addBlock(block);			
			System.out.println("\nWalletA's balance is: " + walletA.getBalance().toString());
			System.out.println("WalletB's balance is: " + walletB.getBalance().toString());
			System.out.println();
		}

		isChainValid();

	}

	private static void generateGenesisBlock() { //Hardcoded addition for the genesis block
		Wallet coinbase = new Wallet();

		//create genesis transaction, which sends 100 coins to walletA: 
		genesisTransaction = new Transaction(coinbase.publicKey, walletA.address, new BigDecimal(100), null); //TODO: Set limit decimals number
		genesisTransaction.generateSignature(coinbase.privateKey);	 //manually sign the genesis transaction	
		genesisTransaction.transactionId = "0"; //manually set the transaction id
		genesisTransaction.outputs.add(new TransactionOutput(genesisTransaction.transactionId,genesisTransaction.reciepient, genesisTransaction.value)); //manually add the Transactions Output
		UTXOs.put(genesisTransaction.outputs.get(0).id, genesisTransaction.outputs.get(0)); //its important to store our first transaction in the UTXOs list.


		System.out.println("Creating and Mining Genesis block... ");
		Block genesis = new Block("0");
		genesis.addTransaction(genesisTransaction);

		genesis.mineBlock(difficulty);
		blockchain.add(genesis);

		//Add the block to the local DB
		Document blockDocument = genesis.getBlockDocument();
		dbManager.insertOne("bloques", blockDocument);

		storeTransactions(genesis);	

		//Add the first UTXO
		ArrayList<Transaction> transactions = genesis.transactions;
		if(transactions!=null) { //FIXME: IT IS  NECCESSARY?
			for(Transaction tx : transactions) {
				//Add new transaction outputs
				dbManager.insertMany("utxos", tx.getTransactionOutputsDocument());
			}
		}

		System.out.println("\nWalletA's balance is: " + walletA.getBalance());
		System.out.println("WalletB's balance is: " + walletB.getBalance());
	}

	public static Boolean isChainValid() {
		Block currentBlock; 
		Block previousBlock;
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		HashMap<String,TransactionOutput> tempUTXOs = new HashMap<String,TransactionOutput>(); //a temporary working list of unspent transactions at a given block state.
		tempUTXOs.put(genesisTransaction.outputs.get(0).id, genesisTransaction.outputs.get(0));

		//loop through blockchain to check hashes:
		for(int i=1; i < blockchain.size(); i++) {

			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i-1);
			//compare registered hash and calculated hash:
			if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
				System.out.println("#Current Hashes not equal");
				return false;
			}
			//compare previous hash and registered previous hash
			if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
				System.out.println("#Previous Hashes not equal");
				return false;
			}
			//check if hash is solved
			if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) {
				System.out.println("#This block hasn't been mined");
				return false;
			}

			//loop thru blockchain transactions:
			TransactionOutput tempOutput;
			for(int t=0; t <currentBlock.transactions.size(); t++) {
				Transaction currentTransaction = currentBlock.transactions.get(t);

				if(!currentTransaction.verifiySignature()) {
					System.out.println("#Signature on Transaction(" + t + ") is Invalid");
					return false; 
				}
				if(currentTransaction.getInputsValue().compareTo(currentTransaction.getOutputsValue())!=0) {
					System.out.println("#Inputs are note equal to outputs on Transaction(" + t + ") and Block: ("+i+") "+currentBlock.hash);
					return false; 
				}

				for(TransactionInput input: currentTransaction.inputs) {	
					tempOutput = tempUTXOs.get(input.transactionOutputId);

					if(tempOutput == null) {
						System.out.println("#Referenced input on Transaction(" + t + ") is Missing");
						return false;
					}

					if(input.UTXO.value != tempOutput.value) {
						System.out.println("#Referenced input Transaction(" + t + ") value is Invalid");
						return false;
					}

					tempUTXOs.remove(input.transactionOutputId);
				}

				for(TransactionOutput output: currentTransaction.outputs) {
					tempUTXOs.put(output.id, output);
				}

				if( !currentTransaction.outputs.get(0).reciepient.equals(currentTransaction.reciepient)) {
					System.out.println("#Transaction(" + t + ") output reciepient is not who it should be");
					return false;
				}
				if( !currentTransaction.outputs.get(1).reciepient.equals(StringUtil.getAddress(currentTransaction.sender))) {
					System.out.println("#Transaction(" + t + ") output 'change' is not sender.");
					return false;
				}

			}

		}
		System.out.println("Blockchain is valid");
		return true;
	}

	public static void addBlock(Block newBlock) {
		newBlock.mineBlock(difficulty);
		blockchain.add(newBlock);

		//Add the block to the local DB
		Document blockDocument = newBlock.getBlockDocument();
		dbManager.insertOne("bloques", blockDocument);

		storeTransactions(newBlock);		
		updateUTXOs(newBlock);
	}

	private static void updateUTXOs(Block newBlock) {
		ArrayList<Transaction> transactions = newBlock.transactions;
		if(transactions!=null) {
			for(Transaction tx : transactions) {
				//Delete transaction outputs consumed
				for(TransactionInput input : tx.inputs) {
					Bson query = eq("id", input.transactionOutputId);
					DeleteResult result = dbManager.deleteOne("utxos", query);
					if(result.getDeletedCount() == 0) {
						throw new RuntimeException("Deleted document count: " + result.getDeletedCount()+ "/t And should not have been 0");
					}
				}
				//Add new transaction outputs
				dbManager.insertMany("utxos",tx.getTransactionOutputsDocument());
			}
		}
	}

	private static void storeTransactions(Block newBlock) {
		List<Document> transactions = newBlock.getTransactionsDocument();
		if(transactions !=null && !transactions.isEmpty()) {
			dbManager.insertMany("transacciones", transactions);
		}
	}




}
