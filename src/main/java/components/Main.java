package components;

import java.net.UnknownHostException;
import java.security.Security;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class Main {


	public static void main(String[] args) {
		MongoClient mongoClient=null;
		mongoClient = new MongoClient();
		DB database = mongoClient.getDB("Pruebas");
		DBCollection collection = database.getCollection("bloques");
		
		DBObject dbObj = new BasicDBObject("previousHash",0);
		DBCursor cursor = collection.find(dbObj);

		System.out.println();
	    try {
	       while(cursor.hasNext()) {
	          DBObject dbobj = cursor.next();
	        //Converting BasicDBObject to a custom Class(Employee)
//	          Block block = (new Gson()).fromJson(dbobj.toString(), Block.class);
	          System.out.println((new Gson()).fromJson(dbobj.toString(), Block.class));
//	          System.out.println(block.hash);
	       }
	    } finally {
	       cursor.close();
	    }
	}

}
