package components;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;

import utils.StringUtil;

public class Block {

	public String hash;
	public String previousHash; 
	public String merkleRoot;
	public ArrayList<Transaction> transactions = new ArrayList<Transaction>(); //our data will be a simple message.
	public long timeStamp; //as number of milliseconds since 1/1/1970.
	public int nonce;

	//Block Constructor.  
	public Block(String previousHash ) { // Review block fields
		this.previousHash = previousHash;
		this.timeStamp = new Date().getTime();
	}

	//Calculate new hash based on blocks contents
	public String calculateHash() {
		String calculatedhash = StringUtil.applySha256( 
				previousHash +
				Long.toString(timeStamp) +
				Integer.toString(nonce) + 
				merkleRoot
				);
		return calculatedhash;
	}

	public void mineBlock(int difficulty) {
		merkleRoot = StringUtil.getMerkleRoot(transactions);
		String target = StringUtil.getDificultyString(difficulty); //Create a string with difficulty * "0" 
		while(hash == null || !hash.substring( 0, difficulty).equals(target)) {
			++nonce;
			hash = calculateHash();
		}
		System.out.println("Block Mined!!! : " + hash);
	}

	//Add transactions to this block
	public boolean addTransaction(Transaction transaction) {
		//process transaction and check if valid, unless block is genesis block then ignore.
		if(transaction == null) return false;		
		if((previousHash != "0")) {
			if((transaction.processTransaction() != true)) {
				System.out.println("Transaction failed to process. Discarded.");
				return false;
			}
		}//FIXME: add an remove UTXOs from DB and add the new transaction to transactions, then add here the transaction
		transactions.add(transaction);
		System.out.println("Transaction Successfully added to Block");
		return true;
	}

	public Document getBlockDocument(){

		Document document = new Document()
				.append("hash", hash)
				.append("previousHash", previousHash)
				.append("merkleRoot", merkleRoot)
				.append("timestamp", timeStamp)
				.append("nonce", nonce);

		//		BasicDBList transactionsArray = new BasicDBList();
		//		for(int i = 0 ; i < transactions.size();i++) {
		//			transactionsArray.add(transactions.get(i).getTransactionDocument());
		//		}
		//		document.append("transactions", transactionsArray);



		return document;
	}

	public List<Document> getTransactionsDocument() {
		List<Document> transactionsDocument = new ArrayList<Document>(); 

		for(int i = 0 ; i < transactions.size();i++) {
			transactionsDocument.add(transactions.get(i).getTransactionDocument());
		}

		return transactionsDocument;	
	}

}
