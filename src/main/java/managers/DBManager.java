package managers;

import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.DeleteResult;

import components.Wallet;

public class DBManager {

	private MongoClient mongoClient;
	private MongoDatabase database;

	public DBManager(){
		this.mongoClient =  MongoClients.create("mongodb://localhost:27017");
		this.database = mongoClient.getDatabase("Pruebas");
	}

	public void insertOne(String collection, Document document) {
		MongoCollection<Document> col = database.getCollection(collection);
		col.insertOne(document);
	}

	public void findWallet(Wallet wallet, String id) {
	}

	public Document findFirst(String collection, Document docToFind) {
		MongoCollection<Document> col = database.getCollection(collection);
		Document wallet = col.find(docToFind).first();

		return wallet;
	}

	public FindIterable<Document> findAll(String collection, Bson filter){
		MongoCollection<Document> col = database.getCollection(collection);
		FindIterable<Document> iter = col.find(filter);
		return iter;
	}

	public void replaceOne(String collection, Bson query, Document docToReplace, ReplaceOptions opts) {
		MongoCollection<Document> col = database.getCollection(collection);
		col.replaceOne(query, docToReplace, opts); //TODO: Maybe the function should return something (UppdateResult)
	}

	public DeleteResult deleteOne(String collection,Bson query) {
		MongoCollection<Document> col = database.getCollection(collection);
		DeleteResult result = col.deleteOne(query);
		return result;
	}

	public void insertMany(String collection, List<Document> docsToInsert) {
		MongoCollection<Document> col = database.getCollection(collection);
		col.insertMany(docsToInsert);
	}

}
